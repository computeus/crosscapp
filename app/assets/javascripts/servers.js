$(document).on('ready, page:change', function () {
    $('.cpu_data, .disk_data').sparkline('html', {
        type: 'bar',
        barColor: 'green',
        disableTooltips: true,
        barWidth: 8,
        chartRangeMin: 0,
        chartRangeMax: 100
    });

    $('.process_data').sparkline('html', {
        type: 'bar',
        barColor: 'green',
        disableTooltips: true,
        barWidth: 8
    });

    new JustGage({id: "cpu_gauge", value: $('#cpu_gauge').data('current-value'), min: 0, max: 100, title: "CPU Load"});
    new JustGage({
        id: "disk_gauge",
        value: $('#disk_gauge').data('current-value'),
        min: 0,
        max: 100,
        title: "Disk Usage"
    });
    new JustGage({
        id: "process_gauge",
        value: $('#process_gauge').data('current-value'),
        min: 0,
        max: 200,
        title: "Process Count"
    });
    new JustGage({
        id: "average_cpu_gauge",
        value: $('#average_cpu_gauge').data('current-value'),
        min: 0,
        max: 100,
        title: "CPU Load"
    });
    new JustGage({
        id: "average_disk_gauge",
        value: $('#average_disk_gauge').data('current-value'),
        min: 0,
        max: 100,
        title: "Disk Usage"
    });
    new JustGage({
        id: "average_process_gauge",
        value: $('#average_process_gauge').data('current-value'),
        min: 0,
        max: 200,
        title: "Process Count"
    });
});