# Generates a unique token for included model.
module Tokenable
  extend ActiveSupport::Concern

  included do
    before_validation :generate_token
  end

  protected

  def generate_token
    self.api_token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless self.class.exists?(api_token: random_token)
    end if api_token.blank?
  end
end