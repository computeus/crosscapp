class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  include Tokenable

  validates :api_token, presence: true

  has_many :servers, inverse_of: :user
  has_many :cpu_data, through: :servers
  has_many :disk_data, through: :servers
  has_many :process_data, through: :servers

  # Gets the cpu_data > 80 from users servers
  def high_cpu_usage_data
    cpu_data.where('cpu_usage > 80').includes(:server).order(created_at: :desc)
  end

  # Gets the disk_data > 80 from users servers
  def high_disk_usage_data
    disk_data.where('disk_usage > 80').includes(:server).order(created_at: :desc)
  end

  # Gets the process_count > 15 from users servers
  def high_process_count_data
    process_data.where('process_count > 15').includes(:server).order(created_at: :desc)
  end
end
