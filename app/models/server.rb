# This class holds the server objects
# validates hostname and user presence
class Server < ActiveRecord::Base
  validates :hostname, presence: true
  validates :user, presence: true

  belongs_to :user, inverse_of: :servers

  has_many :cpu_data, class_name: :ServerCpuDatum, inverse_of: :server
  has_many :disk_data, class_name: :ServerDiskDatum, inverse_of: :server
  has_many :process_data, class_name: :ServerProcessDatum, inverse_of: :server

  attr_accessor :clear_data

  before_save :clear_server_data

  def to_json(*a)
    { 'hostname' => hostname }.to_json(*a)
  end

  # Clears the all server data if user checked the option
  # on server edit form
  def clear_server_data
    if @clear_data == '1'
      cpu_data.each(&:destroy)
      disk_data.each(&:destroy)
      process_data.each(&:destroy)
    end
  end

  # Returns the latest cpu usage data retrieved from server
  def current_cpu_usage
    cpu_data.latest_usage || 0
  end

  # Returns the latest disk usage data retrieved from server
  def current_disk_usage
    disk_data.latest_usage || 0
  end

  # Returns the latest process count data retrieved from server
  def current_process_count
    process_data.latest_process_count || 0
  end

  # Returns the last 10 cpu data cpu usage in reverse order
  # return 10,12,15,13
  def mini_cpu_graph_data
    cpu_data.descending.limit(10).pluck(:cpu_usage).reverse.join(',')
  end

  # Returns the last 30 cpu data cpu usage
  def cpu_graph_data
    cpu_data.descending.limit(30).pluck(:cpu_usage).reverse
  end

  # Returns the last 10 disk data disk usage in reverse order
  # return 10,12,15,13
  def mini_disk_graph_data
    disk_data.descending.limit(10).pluck(:disk_usage).reverse.join(',')
  end

  # Returns the last 30 disk data disk usage
  def disk_graph_data
    disk_data.descending.limit(30).pluck(:disk_usage).reverse
  end

  # Returns the last 10 process data process count in reverse order
  # return 10,12,15,13
  def mini_process_count_data
    process_data.descending.limit(10).pluck(:process_count).reverse.join(',')
  end

  # Returns the last 30 process data process count in reverse order
  def process_count_data
    process_data.descending.limit(30).pluck(:process_count).reverse
  end

  # Returns the average cpu usage
  # If no cpu usage data exists returns 0
  def average_cpu_usage
    cpu_data.average(:cpu_usage).to_i || 0
  end

  # Returns the average disk usage
  # If no disk usage data exists returns 0
  def average_disk_usage
    disk_data.average(:disk_usage).to_i || 0
  end

  # Returns the average process count
  # If no process count data exists returns 0
  def average_process_count
    process_data.average(:process_count).to_i || 0
  end

  # Returns the user created custom name.
  # If no custom name returns the servers hostname
  def name
    custom_name || hostname
  end
end
