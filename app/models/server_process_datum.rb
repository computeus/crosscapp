# Holds the Process Data gathered from server

class ServerProcessDatum < ActiveRecord::Base
  belongs_to :server, inverse_of: :process_data

  has_many :process_data, inverse_of: :server_process_datum, dependent: :destroy

  validates :server, presence: true
  validates :process_count, presence: true

  def to_json(*a)
    { server: server.hostname, process_count: process_count, created_at: created_at }.to_json(*a)
  end

  # Saves the given process data hash array to database
  # process_array = [{'process_name' => 'process_1', 'process_cpu_usage' => '10'}, {'process_name' => 'process_2', 'process_cpu_usage' => '20'}]
  def save_process_data(process_array)
    process_array.each do |process|
      process_data.create(process_name: process['process_name'], process_cpu_usage: process['process_cpu_usage'])
    end
  end

  # Gets the latest created server process datum
  def self.latest
    descending.first
  end

  # Gets the latest created server process data process count
  def self.latest_process_count
    latest.process_count if latest
  end

  # Sorts by created_at field in descending order
  def self.descending
    order(created_at: :desc)
  end
end
