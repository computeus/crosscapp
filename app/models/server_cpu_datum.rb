# Holds the CPU Data gathered from server

class ServerCpuDatum < ActiveRecord::Base
  belongs_to :server, inverse_of: :cpu_data

  validates :server, presence: true
  validates :cpu_usage, presence: true

  def to_json(*a)
    { server: server.hostname, cpu_usage: cpu_usage, created_at: created_at }.to_json(*a)
  end

  # Sorts by created_at field in descending order
  def self.descending
    order(created_at: :desc)
  end

  # Gets the latest created server cpu datum
  def self.latest
    descending.first
  end

  # Gets the latest created server cpu data cpu usage
  def self.latest_usage
    latest.cpu_usage if latest
  end
end
