# Holds the CPU Data gathered from server

class ServerDiskDatum < ActiveRecord::Base
  belongs_to :server, inverse_of: :disk_data

  validates :server, presence: true
  validates :disk_usage, presence: true

  def to_json(*a)
    { server: server.hostname, disk_usage: disk_usage, created_at: created_at }.to_json(*a)
  end

  # Gets the latest created server disk datum
  def self.latest
    descending.first
  end

  # Gets the latest created server disk data disk usage
  def self.latest_usage
    latest.disk_usage if latest
  end

  # Sorts by created_at field in descending order
  def self.descending
    order(created_at: :desc)
  end
end
