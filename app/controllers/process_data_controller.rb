class ProcessDataController < ApplicationController
  before_action :set_server

  # GET /process_data
  # GET /process_data.json
  def index
    @process_data = @server.process_data.descending.page params[:page]

    @process_chart = LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: 'Last 30 Process Count Graph')
      f.series(name: 'Process Count', yAxis: 0, data: @server.process_count_data)
      f.chart({ defaultSeriesType: 'area' })
    end
  end

  # GET /process_data/1
  # GET /process_data/1.json
  def show
    @process_datum = @server.process_data.find(params[:id])
    @process_data = @process_datum.process_data
  end

  private
    def set_server
      @server = current_user.servers.find(params[:server_id])
    end
end
