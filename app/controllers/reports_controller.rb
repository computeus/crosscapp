class ReportsController < ApplicationController
  def cpu
    @cpu_data = current_user.high_cpu_usage_data

    responder_method('cpu')
  end

  def disk
    @disk_data = current_user.high_disk_usage_data

    responder_method('disk')
  end

  def process_report
    @process_data = current_user.high_process_count_data

    responder_method('process_report')
  end

  private
  def responder_method(file)
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: file, file: "reports/#{file}"
      end
    end
  end
end
