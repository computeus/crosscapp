class DiskDataController < ApplicationController
  # GET /cpu_data
  # GET /cpu_data.json
  def index
    @server = current_user.servers.find(params[:server_id])

    @disk_data = @server.disk_data.descending.page params[:page]

    @disk_chart = LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: 'Last 30 Disk Data Graph')
      f.series(name: 'Disk Usage Percentage', yAxis: 0, data: @server.disk_graph_data)
      f.chart({ defaultSeriesType: 'area' })
    end
  end
end
