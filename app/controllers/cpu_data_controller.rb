class CpuDataController < ApplicationController
  # GET /cpu_data
  # GET /cpu_data.json
  def index
    @server = current_user.servers.find(params[:server_id])

    @cpu_data = @server.cpu_data.descending.page params[:page]

    @cpu_chart = LazyHighCharts::HighChart.new('graph') do |f|
      f.title(text: 'Last 30 CPU Data Graph')
      f.series(name: 'Cpu Usage Percentage', yAxis: 0, data: @server.cpu_graph_data)
      f.chart({ defaultSeriesType: 'area' })
    end
  end
end
