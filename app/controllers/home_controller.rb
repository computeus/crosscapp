class HomeController < ApplicationController
  skip_before_filter :authenticate_user!

  def welcome
    redirect_to servers_path if user_signed_in?
  end
end
