class ServersController < ApplicationController
  before_action :set_server, only: [:show, :edit, :update]

  # GET /servers
  # GET /servers.json
  def index
    @servers = current_user.servers.all
  end

  # GET /servers/1
  # GET /servers/1.json
  def show
  end

  # GET /servers/1/edit
  def edit
  end

  # PATCH/PUT /servers/1
  # PATCH/PUT /servers/1.json
  def update
    respond_to do |format|
      if @server.update(server_params)
        format.html { redirect_to server_path(@server), notice: 'Server was successfully updated.' }
        format.json { render :show, status: :ok, location: @server }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_server
    @server = current_user.servers.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def server_params
    params.require(:server).permit(:custom_name, :clear_data, :cpu_alert, :disk_alert, :process_alert)
  end
end
