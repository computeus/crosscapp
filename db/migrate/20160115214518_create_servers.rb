class CreateServers < ActiveRecord::Migration
  def change
    create_table :servers do |t|
      t.integer :user_id
      t.string :hostname

      t.timestamps null: false
    end
    add_index :servers, :user_id
    add_index :servers, :hostname
  end
end
