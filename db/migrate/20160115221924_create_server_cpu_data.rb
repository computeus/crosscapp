class CreateServerCpuData < ActiveRecord::Migration
  def change
    create_table :server_cpu_data do |t|
      t.integer :server_id
      t.integer :cpu_usage

      t.timestamps null: false
    end
    add_index :server_cpu_data, :server_id
    add_index :server_cpu_data, :cpu_usage
  end
end
