class AddCpuAlertDiskAlertProcessAlertToServer < ActiveRecord::Migration
  def change
    add_column :servers, :cpu_alert, :boolean, default: false
    add_column :servers, :disk_alert, :boolean, default: false
    add_column :servers, :process_alert, :boolean, default: false
  end
end
