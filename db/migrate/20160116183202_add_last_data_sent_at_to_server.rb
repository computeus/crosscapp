class AddLastDataSentAtToServer < ActiveRecord::Migration
  def change
    add_column :servers, :last_data_sent_at, :datetime
    add_index :servers, :last_data_sent_at
  end
end
