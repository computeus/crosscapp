class CreateProcessData < ActiveRecord::Migration
  def change
    create_table :process_data do |t|
      t.integer :server_process_datum_id
      t.integer :process_cpu_usage
      t.string :process_name

      t.timestamps null: false
    end
    add_index :process_data, :server_process_datum_id
    add_index :process_data, :process_cpu_usage
  end
end
