class AddCustomNameToServer < ActiveRecord::Migration
  def change
    add_column :servers, :custom_name, :string
  end
end
