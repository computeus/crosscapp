class CreateServerProcessData < ActiveRecord::Migration
  def change
    create_table :server_process_data do |t|
      t.integer :server_id
      t.integer :process_count

      t.timestamps null: false
    end
    add_index :server_process_data, :server_id
    add_index :server_process_data, :process_count
  end
end
