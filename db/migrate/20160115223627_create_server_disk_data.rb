class CreateServerDiskData < ActiveRecord::Migration
  def change
    create_table :server_disk_data do |t|
      t.integer :server_id
      t.integer :disk_usage

      t.timestamps null: false
    end
    add_index :server_disk_data, :server_id
    add_index :server_disk_data, :disk_usage
  end
end
