class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :api_token

      t.timestamps null: false
    end
    add_index :users, :api_token
  end
end
