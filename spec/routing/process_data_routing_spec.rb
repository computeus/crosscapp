require 'rails_helper'

RSpec.describe ProcessDataController, type: :routing do
  describe 'routing' do

    it 'routes to #index' do
      expect(get: '/servers/1/process_data').to route_to('process_data#index', server_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/servers/1/process_data/2').to route_to('process_data#show', id: '2', server_id: '1')
    end
  end
end
