require 'rails_helper'

RSpec.describe DiskDataController, type: :routing do
  describe 'routing' do

    it 'routes to #index' do
      expect(get: '/servers/1/disk_data').to route_to('disk_data#index', server_id: '1')
    end
  end
end
