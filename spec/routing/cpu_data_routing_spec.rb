require 'rails_helper'

RSpec.describe CpuDataController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/servers/1/cpu_data').to route_to('cpu_data#index', server_id: '1')
    end
  end
end
