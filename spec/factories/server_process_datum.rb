FactoryGirl.define do
  factory :server_process_datum do
    process_count { rand(100) }
    server
  end
end