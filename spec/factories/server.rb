FactoryGirl.define do
  factory :server do
    hostname { 'hosthost' }
    custom_name { 'custom_name' }
    user
  end
end