FactoryGirl.define do
  factory :server_cpu_datum do
    cpu_usage { rand(100) }
    server
  end
end