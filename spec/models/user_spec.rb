require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#high_cpu_usage_data' do
    it 'should return all high cpu usage data' do
      server = FactoryGirl.create :server

      data1 = FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 90, created_at: Time.now - 2.day
      data2 = FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 90
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 70

      server.user.high_cpu_usage_data.should eq([data2, data1])
    end
  end

  describe '#high_disk_usage_data' do
    it 'should return all high disk usage data' do
      server = FactoryGirl.create :server

      data1 = FactoryGirl.create :server_disk_datum, server: server, disk_usage: 90, created_at: Time.now - 2.day
      data2 = FactoryGirl.create :server_disk_datum, server: server, disk_usage: 90
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 70

      server.user.high_disk_usage_data.should eq([data2, data1])
    end
  end

  describe '#high_process_count_data' do
    it 'should return all high process count data' do
      server = FactoryGirl.create :server

      data1 = FactoryGirl.create :server_process_datum, server: server, process_count: 17, created_at: Time.now - 2.day
      data2 = FactoryGirl.create :server_process_datum, server: server, process_count: 17
      FactoryGirl.create :server_process_datum, server: server, process_count: 10

      server.user.high_process_count_data.should eq([data2, data1])
    end
  end
end
