require 'rails_helper'

RSpec.describe ServerCpuDatum, type: :model do
  let(:server_cpu_datum) { FactoryGirl.create :server_cpu_datum }
  it { should validate_presence_of :server }
  it { should validate_presence_of :cpu_usage }

  describe '.descending' do
    it 'should order created_at desc' do
      data1 = FactoryGirl.create :server_cpu_datum, created_at: Time.now - 1.day
      data2 = FactoryGirl.create :server_cpu_datum

      ServerCpuDatum.descending.should eq([data2, data1])
    end
  end

  describe '.latest' do
    it 'should return latest created server cpu datum' do
      FactoryGirl.create :server_cpu_datum, created_at: Time.now - 1.day
      data2 = FactoryGirl.create :server_cpu_datum

      ServerCpuDatum.latest.should eq(data2)
    end
  end

  describe '.latest_usage' do
    it 'should return latest created server cpu datum cpu usage attribute' do
      FactoryGirl.create :server_cpu_datum, created_at: Time.now - 1.day
      FactoryGirl.create :server_cpu_datum, cpu_usage: 44

      ServerCpuDatum.latest_usage.should eq(44)
    end
  end

  describe '#to_json' do
    it 'should return server_name, cpu_usage and created at attributes' do
      attributes = JSON.parse(server_cpu_datum.to_json)
      attributes.count.should eq(3)
      attributes.has_key?('server').should eq(true)
      attributes.has_key?('cpu_usage').should eq(true)
      attributes.has_key?('created_at').should eq(true)
    end
  end
end
