require 'rails_helper'

RSpec.describe Server, type: :model do
  let(:server) { FactoryGirl.create :server }
  it { should validate_presence_of :hostname }
  it { should validate_presence_of :user }

  describe '#to_json' do
    it 'should return hostname attributes' do
      attributes = JSON.parse(server.to_json)
      attributes.count.should eq(1)
      attributes.has_key?('hostname').should eq(true)
    end
  end

  describe '#name' do
    it 'should return custom_name or hostname attributes' do
      server = FactoryGirl.create :server, hostname: 'server_hostname', custom_name: nil
      server.name.should eq('server_hostname')

      server = FactoryGirl.create :server, hostname: 'server_hostname', custom_name: 'custom_name'
      server.name.should eq('custom_name')
    end
  end

  describe '#average_process_count' do
    it 'should return average process count' do
      FactoryGirl.create :server_process_datum, server: server, process_count: 3
      FactoryGirl.create :server_process_datum, server: server, process_count: 5

      server.average_process_count.should eq(4)
    end
  end

  describe '#average_disk_usage' do
    it 'should return disk usage count' do
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 3
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 5

      server.average_disk_usage.should eq(4)
    end
  end

  describe '#average_cpu_usage' do
    it 'should return cpu usage count' do
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 3
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 5

      server.average_cpu_usage.should eq(4)
    end
  end

  describe '#process_count_data' do
    it 'should return reverse process count of latest 30 data' do
      FactoryGirl.create :server_process_datum, server: server, process_count: 4, created_at: Time.now
      FactoryGirl.create :server_process_datum, server: server, process_count: 3, created_at: Time.now - 1.day
      FactoryGirl.create :server_process_datum, server: server, process_count: 5, created_at: Time.now - 2.day

      server.process_count_data.should eq([5, 3, 4])

      50.times do |i|
        FactoryGirl.create :server_process_datum, server: server
      end

      server.process_count_data.count.should eq(30)
    end
  end

  describe '#mini_process_count_data' do
    it 'should return reverse process count of latest 10 data joined with ,' do
      FactoryGirl.create :server_process_datum, server: server, process_count: 4, created_at: Time.now
      FactoryGirl.create :server_process_datum, server: server, process_count: 3, created_at: Time.now - 1.day
      FactoryGirl.create :server_process_datum, server: server, process_count: 5, created_at: Time.now - 2.day

      server.mini_process_count_data.should eq('5,3,4')

      20.times do |i|
        FactoryGirl.create :server_process_datum, server: server
      end

      server.mini_process_count_data.split(',').count.should eq(10)
    end
  end

  describe '#disk_graph_data' do
    it 'should return reverse disk usage of latest 30 data' do
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 4, created_at: Time.now
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 3, created_at: Time.now - 1.day
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 5, created_at: Time.now - 2.day

      server.disk_graph_data.should eq([5, 3, 4])

      50.times do |i|
        FactoryGirl.create :server_disk_datum, server: server
      end

      server.disk_graph_data.count.should eq(30)
    end
  end

  describe '#mini_disk_graph_data' do
    it 'should return reverse disk usage of latest 10 data joined with ,' do
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 4, created_at: Time.now
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 3, created_at: Time.now - 1.day
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 5, created_at: Time.now - 2.day

      server.mini_disk_graph_data.should eq('5,3,4')

      20.times do |i|
        FactoryGirl.create :server_disk_datum, server: server
      end

      server.mini_disk_graph_data.split(',').count.should eq(10)
    end
  end

  describe '#cpu_graph_data' do
    it 'should return reverse cpu usage of latest 30 data' do
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 4, created_at: Time.now
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 3, created_at: Time.now - 1.day
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 5, created_at: Time.now - 2.day

      server.cpu_graph_data.should eq([5, 3, 4])

      50.times do |i|
        FactoryGirl.create :server_cpu_datum, server: server
      end

      server.cpu_graph_data.count.should eq(30)
    end
  end

  describe '#mini_cpu_graph_data' do
    it 'should return reverse cpu usage of latest 10 data joined with ,' do
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 4, created_at: Time.now
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 3, created_at: Time.now - 1.day
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 5, created_at: Time.now - 2.day

      server.mini_cpu_graph_data.should eq('5,3,4')

      20.times do |i|
        FactoryGirl.create :server_cpu_datum, server: server
      end

      server.mini_cpu_graph_data.split(',').count.should eq(10)
    end
  end

  describe '#current_process_count' do
    it 'should return latest process data' do
      server.current_process_count.should eq(0)

      FactoryGirl.create :server_process_datum, server: server, process_count: 4

      server.current_process_count.should eq(4)
    end
  end

  describe '#current_cpu_usage' do
    it 'should return latest cpu usage' do
      server.current_cpu_usage.should eq(0)

      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 4

      server.current_cpu_usage.should eq(4)
    end
  end

  describe '#current_disk_usage' do
    it 'should return latest disk usage' do
      server.current_disk_usage.should eq(0)

      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 4

      server.current_disk_usage.should eq(4)
    end
  end

  describe '#clear_server_data' do
    it 'should clear server data if clear data is set' do
      FactoryGirl.create :server_cpu_datum, server: server, cpu_usage: 4
      FactoryGirl.create :server_disk_datum, server: server, disk_usage: 4
      FactoryGirl.create :server_process_datum, server: server, process_count: 4

      server.cpu_data.count.should eq(1)
      server.disk_data.count.should eq(1)
      server.process_data.count.should eq(1)

      server.clear_server_data

      server.cpu_data.count.should eq(1)
      server.disk_data.count.should eq(1)
      server.process_data.count.should eq(1)

      server.clear_data = '1'
      server.clear_server_data

      server.cpu_data.count.should eq(0)
      server.disk_data.count.should eq(0)
      server.process_data.count.should eq(0)
    end
  end
end