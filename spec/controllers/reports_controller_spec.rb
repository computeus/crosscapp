require 'rails_helper'

RSpec.describe ReportsController, type: :controller do
  let(:server) { FactoryGirl.create :server }

  before(:each) do
    sign_in server.user
  end

  describe 'GET #cpu' do
    it 'returns http success' do
      get :cpu
      expect(response).to have_http_status(:success)
    end

    it 'returns http success and renders pdf' do
      get :cpu, format: :pdf
      expect(response).to have_http_status(:success)
      expect(response).to render_template('reports/cpu')
      response.header['Content-Type'].should eq('application/pdf')
    end
  end

  describe 'GET #disk' do
    it 'returns http success' do
      get :disk
      expect(response).to have_http_status(:success)
    end

    it 'returns http success and renders pdf' do
      get :disk, format: :pdf
      expect(response).to have_http_status(:success)
      expect(response).to render_template('reports/disk')
      response.header['Content-Type'].should eq('application/pdf')
    end
  end

  describe 'GET #data' do
    it 'returns http success' do
      get :process_report
      expect(response).to have_http_status(:success)
    end

    it 'returns http success and renders pdf' do
      get :process_report, format: :pdf
      expect(response).to have_http_status(:success)
      expect(response).to render_template('reports/process_report')
      response.header['Content-Type'].should eq('application/pdf')
    end
  end
end
