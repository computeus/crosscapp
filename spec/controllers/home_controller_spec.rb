require 'rails_helper'

RSpec.describe HomeController, type: :controller do

  describe 'GET #welcome' do
    it 'returns http success' do
      get :welcome
      expect(response).to have_http_status(:success)
    end

    it 'redirects if user is signed in' do
      user = FactoryGirl.create :user
      sign_in user
      get :welcome
      expect(response).to have_http_status(:redirect)
    end
  end
end
