require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe ProcessDataController, type: :controller do
  # This should return the minimal set of attributes required to create a valid
  # ProcessDatum. As you add validations to ProcessDatum, be sure to
  # adjust the attributes here as well.

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # ProcessDataController. Be sure to keep this updated too.
  let(:valid_session) { {} }
  let(:server) { FactoryGirl.create :server }
  let(:process_datum) { FactoryGirl.create :server_process_datum, server: server }
  let(:process_data) { FactoryGirl.create :process_datum, server_process_datum: process_datum }

  before(:each) do
    sign_in server.user
  end

  describe 'GET #index' do
    it 'assigns all process_data as @process_data' do
      get :index, { server_id: server.to_param }, valid_session
      expect(assigns(:process_data)).to eq([process_datum])
    end
  end

  describe 'GET #show' do
    it 'assigns the requested server as @server' do

      get :show, { id: process_datum.to_param, server_id: server.to_param }, valid_session
      expect(assigns(:process_datum)).to eq(process_datum)
      expect(assigns(:process_data)).to eq([process_data])
    end
  end
end
