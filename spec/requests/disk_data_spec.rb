require 'rails_helper'

RSpec.describe 'DiskData', type: :request do
  let(:server) { FactoryGirl.create :server }

  before(:each) do
    login_as server.user, scope: :user
  end

  describe 'GET /disk_data' do
    it 'works! (now write some real specs)' do
      get server_disk_data_path(server)
      expect(response).to have_http_status(200)
    end
  end
end
